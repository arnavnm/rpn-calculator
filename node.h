
#ifndef PS5_NODE_H
#define PS5_NODE_H

#define ADDORSUB 1
#define MULTORDIV 2
#define POW 3

enum type {OPERATOR, NUMBER};

typedef struct node{
    union {
        double value;
        char operator;
    } contents;
    int type;
    int precedence;
    struct node *next;
}node;


node* createNode(char*, enum type);



#endif //PS5_NODE_H
