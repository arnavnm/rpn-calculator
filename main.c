#include <stdio.h>
#include "rpn.h"
#include "errors.h"
#include <string.h>



int main() {
    char expression[300]="";

    FILE* fileIn = fopen("./ps5-infile.txt", "r");

    FILE* fileOut = fopen("/home/cs210/cs210/PS5/ps5-outfile.txt","w+");

    char line[300];

    if(fileIn == NULL){
        printf("Invalid ");

        return 0;

    }

    while (fgets(line, sizeof(line), fileIn)) {
        line[strlen(line)-2]= '\0';


        strcpy(expression,line);

        fprintf(fileOut,"Infix: %s\n", expression);
        double result = 0;

        status = convert(expression);
        if( status == SUCCESS) {
            fprintf( fileOut,"Postfix: %s\n", expression);

            result = evaluate(expression, &status);

            if(status == SUCCESS)


                fprintf(fileOut,"Result: %f\n\n", result);
            else


                fprintf(fileOut,"Result: %s\n\n", errorMessages[status]);
        }

        else

            fprintf(fileOut,"Result: %s\n\n", errorMessages[status]);

    }

    fclose(fileIn);
    fclose(fileOut);
    return 0;
}
