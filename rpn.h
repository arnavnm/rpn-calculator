

#ifndef PS5_RPN_H
#define PS5_RPN_H

#include "stack.h"
#include "errors.h"

static int status=SUCCESS;
int convert(char*);
double evaluate(char* , int*);

#endif //PS5_RPN_H
