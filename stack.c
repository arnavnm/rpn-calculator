


#include "stack.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "node.h"

node* head = NULL;

bool stackEmpty(){
    if(head==NULL){


        return true;
    }
    else {

        return false;
    }
}

void push(node* n1_ptr){
    if(head == NULL){

        head= n1_ptr;

        return;
    }
    n1_ptr->next= head;

    head= n1_ptr;
}

node* pop(){
    if(head==NULL){
        return NULL;
    }
    node* temp= head;
    char oper[30];


    node* deleted = NULL;
    if(temp->type == NUMBER){

        sprintf(oper, "%lf",temp->contents.value);
    } else {


        oper[0] = temp->contents.operator;
        oper[1] = '\0';
    }
    deleted = createNode(oper, temp->type);

    head= temp->next;

    temp->next= NULL;

    free(temp);
    return deleted;
}

node* peek(){
    return head;
}

void clearStack(){
    node* freen;

    while(head!=NULL){

        freen= head;

        head= head->next;

        freen->next = NULL;
        free(freen);
    }
}

int stackLen(){

    int counter=0;
    node* temp = head;


    while(temp!=NULL){
        counter++;


        temp= temp->next;
    }

    return counter;
}


