

#include "node.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


node* createNode(char* var, enum type typ){
    char* str;
    node* n1= (node*) malloc(sizeof(node));

    if(n1==NULL){

        return NULL;
    }

    if( typ == OPERATOR){

        n1->contents.operator = var[0];


        if(var[0]=='+' || var[0]=='-'){

            n1->precedence = ADDORSUB;

        }
        else if (var[0]=='*' || var[0]=='/'){
            n1-> precedence = MULTORDIV;
        }

        else if     (var[0]=='^'){
            n1->precedence = POW;
        }
    }
    else{
        n1->contents.value = strtod(var,&str);
    }

    n1->next= NULL;
    n1->type = typ;

    return n1;
}

