
#include "rpn.h"
#include <string.h>
#include <ctype.h>
#include "errors.h"
#include "node.h"
#include <stdio.h>

void strinsert(char* source, char* sub, int index )
{
    int len = strlen(sub);
    
    for(int i=0; i<len; i++){
        source[index++] = sub[i];
    }
}

int convert(char* exp) {
    clearStack();

    node *trav;

    node *temp;

    int length = strlen(exp);

    char result[length];

    int index=0;
    memset(result, ' ', length);

    char oper[10];
    char *token = strtok(exp, " ");


    while (token != NULL) {

        strcpy(oper, "  ");

        if (isdigit(token[0])) {
            strcpy(oper, token);

            strcat(oper," ");

            strinsert(result, oper, index);
            index=index+strlen(oper);


        }
        else if (token[0] == '+' || token[0] == '-' || token[0] == '*' || token[0] == '/' || token[0] == '^') {

            trav = createNode(token, OPERATOR);

            while (!stackEmpty() && (peek()->precedence >= trav->precedence)) {


                oper[0] = pop()->contents.operator;

                strinsert(result, oper, index);

                index=index+strlen(oper);

            }
            push(trav);
        }

        else if (token[0] == '(') {

            push(createNode(token, OPERATOR));
        }
        else if (token[0] == ')') {

            while (!stackEmpty() && peek()->contents.operator != '(') {

                oper[0] = pop()->contents.operator;

                strinsert(result, oper, index);

                index=index+strlen(oper);
            }

            if (!stackEmpty() && peek()->contents.operator == '(') {
                pop();

            } else {

                return BRACKETERR;
            }
        }
        else{

            return NOTVALIDOP;
        }

        token = strtok(NULL, " ");
    }

    strcpy(oper,"  ");

    while(!stackEmpty()){
        oper[0] = pop()->contents.operator;

        if(oper[0] == '(') {

            return BRACKETERR;
        }

        strinsert(result, oper, index);

        index=index+strlen(oper);
    }
    result[length] = '\0';

    strcpy(exp, result);

    return SUCCESS;
}


double recursivePower(double n1, double n2){
    
    // calculates power function recursively 
    if(n2==0){

        return 1;
    }

    return n1*recursivePower(n1,n2-1);
}



double evaluate(char* expression, int* status){
    char* token = strtok(expression, " ");

    *status = SUCCESS;

    if(token==NULL){

        *status= NULLPTR;

        return 0.0;
    }


    char* str;
    
    
    double vals;
    
    char temp[30];
    
    
    
    clearStack();
    
    node* l1;
    
    node* r1;

    while(token!=NULL){

        if(token[0]=='-' || token[0]=='+' || token[0]=='*' || token[0]=='/' || token[0]=='^') {
            
            r1=pop();
            
            l1= pop();
            
            if( r1==NULL || l1==NULL){
                *status= TOOFEW;
                
                return 0.0;
                
            }
            switch(token[0]){
                case '+':
                    sprintf(temp, "%lf", (l1->contents.value + r1->contents.value));

                    push(createNode(temp, NUMBER));

                    break;
                case '-':

                    sprintf(temp, "%lf", (l1->contents.value - r1->contents.value));

                    push(createNode(temp, NUMBER));
                    break;
                case '*':

                    sprintf(temp, "%lf", (l1->contents.value * r1->contents.value));

                    push(createNode(temp, NUMBER));

                    break;
                case '/':
                    if(r1->contents.value==0){
                        *status= DIVIDEZERO;

                    }
                    else{
                        sprintf(temp, "%lf", (l1->contents.value / r1->contents.value));

                        push(createNode(temp, NUMBER));
                    }
                    break;

                case '^':
                    sprintf(temp, "%lf", recursivePower(l1->contents.value , r1->contents.value));

                    push(createNode(temp, NUMBER));

                    break;
            }
        }
        else {
            push(createNode(token, NUMBER));
        }

        if(*status){

            return 0.0;
        }

        token= strtok(NULL, " ");
    }

    if(stackLen()>1){

        *status= TOOMANY;

        return 0.0;
    }
    node* result;

    result= peek();

    return result->contents.value;
}