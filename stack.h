

#ifndef PS5_STACK_H
#define PS5_STACK_H

#include "node.h"
#include <stdbool.h>


void push(node*);
node* pop(void);
void clearStack(void);
bool stackEmpty(void);
node* peek(void);
int stackLen(void);


#endif //PS5_STACK_H