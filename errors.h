
#ifndef PS5_ERRORS_H
#define PS5_ERRORS_H

#define   SUCCESS  0
#define   NULLPTR   1
#define   TOOMANY 2
#define  TOOFEW    3
#define DIVIDEZERO 4
#define NOTVALIDOP 5
#define BRACKETERR 6


static char* errorMessages[] = {
        "Calculation successful",
        " null pointer",
        "Too many operands",
        "Too few operands",
        "Cannot divide by Zero",
        "Invalid Operator",
        "Mismatched brackets"
};


#endif //PS5_ERRORS_H